**Building a image off Jenkins with Docker.**

**<p>Building the image</p>**

`docker build . --tag userdockerhub/jenkins:1.0.0 --tag userdockerhub/jenkins:latest`

**<p>Run the container</p>**

`docker run --name docker-jenkins -d -p 8080:8080 -v jenkins_home:/var/jenkins_home -v jenkins_backup:/srv/backup   userdockerhub/jenkins:1.0.0`

**<p>Access on browser</p>**

http://localhost:8080

<p>User: admin</p>
<p>Passsword: admin123</p>

**<p>For push in your docker hub</p>**

`docker push userdockerhub/jenkins:1.0.0`